import { IWeahterState } from './types';
import { LOAD_WEATHER_FAIL, LOAD_WEATHER_SUCEESS, SET_FETCHING, WeatherActionTypes } from '../actions/types';

const initState: IWeahterState = {
    loading: false,
    error: null,
    information: {
        coord: { lon: 14.42, lat: 50.09 },
        weather: [{ id: 803, main: 'Clouds', description: 'broken clouds', icon: '04d' }],
        base: 'stations',
        main: { temp: 7.66, feels_like: 1.56, temp_min: 6.11, temp_max: 9, pressure: 1018, humidity: 75 },
        visibility: 10000,
        wind: { speed: 6.7, deg: 230 },
        clouds: { all: 75 },
        dt: 1578649984,
        sys: { type: 1, id: 6848, country: 'CZ', sunrise: 1578639529, sunset: 1578669593 },
        timezone: 3600,
        id: 3067696,
        name: 'Prague',
        cod: 200,
    },
};

export function weatherReducer(state: IWeahterState = initState, action: WeatherActionTypes): IWeahterState {
    switch (action.type) {
        case SET_FETCHING: {
            return {
                ...state,
                error: action.isFetching && null,
                loading: action.isFetching,
            };
        }
        case LOAD_WEATHER_SUCEESS: {
            return {
                ...state,
                error: null,
                information: action.payload,
            };
        }
        case LOAD_WEATHER_FAIL: {
            return {
                ...state,
                information: initState.information,
                error: action.payload,
            };
        }
        default:
            return state;
    }
}

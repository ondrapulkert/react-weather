import { combineReducers } from 'redux';
import { IWeahterState } from './types';
import { weatherReducer } from './weatherReducer';

export interface AppStore {
    weather: IWeahterState;
}

const rootReducer = combineReducers({
    weather: weatherReducer,
});

export default rootReducer;

const size = {
    mobile: '320px',
    mobileM: '500px',
    tablet: '768px',
    desktop: '1024px',
};

export const device = {
    mobile: `(min-width: ${size.mobile})`,
    mobileM: `(min-width: ${size.mobileM})`,
    tablet: `(min-width: ${size.tablet})`,
    desktop: `(min-width: ${size.desktop})`,
};

import React from 'react';
import { useSelector } from 'react-redux';
import { AppStore } from '../reducers';
import { IWeather } from '../reducers/types';
import { Wrapper } from './styles';
import WeatherCard from './WeatherCard';

const WeatherInformation: React.FC = () => {
    const loading: boolean = useSelector((state: AppStore) => state.weather.loading);
    const information: IWeather = useSelector((state: AppStore) => state.weather.information);
    const error: any = useSelector((state: AppStore) => state.weather.error);

    return (
        <Wrapper>
            {(loading || error) && (
                <div className="notification">
                    {loading && <div>Loading Informatinon...</div>} {error && <div>{error.message}</div>}
                </div>
            )}
            <WeatherCard information={information} error={error} loading={loading} />
        </Wrapper>
    );
};

export default WeatherInformation;

import React from 'react';
import {
    Card,
    CardCoords,
    CardImage,
    CardInfo,
    CardInfoWrapper,
    CardLocation,
    CardTemp,
    CardTitle,
    CardWrapper,
} from './styles';
import { IWeather } from '../reducers/types';

interface WheaterCardProps {
    information: IWeather;
    error: any;
    loading: boolean;
}

const WeatherCard: React.FC<WheaterCardProps> = ({ information, error, loading }) => {
    if (error || loading) {
        return null;
    }
    return (
        <Card>
            <CardWrapper>
                <CardTitle>{information.weather[0].main}</CardTitle>
                <CardLocation>
                    {information.name}, {information.sys.country}
                </CardLocation>
            </CardWrapper>
            <CardInfoWrapper>
                <CardWrapper center={true}>
                    <CardImage
                        src={`http://openweathermap.org/img/wn/${information.weather[0].icon}@2x.png`}
                        alt="icon"
                    />
                    <CardTemp>
                        {Math.round(information.main.temp)}
                        <span>°C</span>
                    </CardTemp>
                </CardWrapper>
                <div>
                    <CardInfo>Humidity: {information.main.humidity}%</CardInfo>
                    <CardInfo>Wind speed: {information.wind.speed} mi/h</CardInfo>
                    <CardInfo>Air pressure: {information.main.pressure} mb</CardInfo>
                </div>
            </CardInfoWrapper>
            <CardCoords>
                <div>
                    Latitude: <span>{information.coord.lat}</span>
                </div>
                <div>
                    Longitude: <span>{information.coord.lon}</span>
                </div>
            </CardCoords>
        </Card>
    );
};

export default WeatherCard;

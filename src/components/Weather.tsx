import React, { useEffect, useState } from 'react';
import { Container, InputHolder } from './styles';
import { useDispatch } from 'react-redux';
import { loadWeatherInformation } from '../actions';
import WeatherInformation from './WeatherInformation';

let timeout = 0;
const Weather: React.FC = () => {
    const [city, setCity] = useState('');
    const dispatch = useDispatch();

    useEffect(() => {
        if (city) {
            dispatch(loadWeatherInformation(city));
        }
    }, [city, dispatch]);

    const handleChange = event => {
        let searchText: string = event.target.value;
        if (timeout) clearTimeout(timeout);
        timeout = setTimeout(() => {
            setCity(searchText);
        }, 500);
    };

    return (
        <Container>
            <InputHolder>
                <input type="text" placeholder="Search" onChange={handleChange} />
                <span>&nbsp;</span>
            </InputHolder>
            <WeatherInformation />
        </Container>
    );
};

export default Weather;

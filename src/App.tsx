import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Weather from './components/Weather';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
    * {
        box-sizing: border-box;
    }
    
    body {
        background: #f5f5f5;
        font-family: 'Montserrat', sans-serif;
        font-weight: normal;
    }
`;

const App: React.FC = () => {
    return (
        <Provider store={store}>
            <GlobalStyle />
            <Weather />
        </Provider>
    );
};

export default App;

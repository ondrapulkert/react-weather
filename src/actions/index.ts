import {
    API_KEY,
    AppThunk,
    LOAD_WEATHER_FAIL,
    LOAD_WEATHER_SUCEESS,
    SET_FETCHING,
    SetFetching,
    WeatherActionTypes,
} from './types';
import { Dispatch } from 'redux';
import { IWeather } from '../reducers/types';
import axios from 'axios';

export const isFetching = (isFetching: boolean): SetFetching => {
    return { type: SET_FETCHING, isFetching };
};

export const loadWeatherInformationSuccess = (information: IWeather): WeatherActionTypes => {
    return {
        type: LOAD_WEATHER_SUCEESS,
        payload: information,
    };
};

export const loadWeatherInformationFail = (error: any): WeatherActionTypes => {
    return {
        type: LOAD_WEATHER_FAIL,
        payload: error,
    };
};

export const loadWeatherInformation = (city: string): AppThunk => {
    return async (dispatch: Dispatch) => {
        try {
            dispatch(isFetching(true));
            const response = await axios.get(
                `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&units=metric`
            );
            const data = await response.data;
            dispatch(isFetching(false));
            dispatch(loadWeatherInformationSuccess(data));
        } catch (error) {
            dispatch(isFetching(false));
            dispatch(loadWeatherInformationFail(error.response.data));
        }
    };
};

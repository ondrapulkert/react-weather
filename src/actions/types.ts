import { ThunkAction } from 'redux-thunk';
import { AppStore } from '../reducers';
import { Action } from 'redux';
import { IWeather } from '../reducers/types';

export const LOAD_WEATHER = 'LOAD_WEATHER';
export const SET_FETCHING = 'SET_FETCHING';
export const LOAD_WEATHER_SUCEESS = 'LOAD_WEATHER_SUCEESS';
export const LOAD_WEATHER_FAIL = 'LOAD_WEATHER_FAIL';

export interface SetFetching {
    type: typeof SET_FETCHING;
    isFetching: boolean;
}
export interface LoadWeather {
    type: typeof LOAD_WEATHER;
    payload: string;
}

export interface LoadWeatherSuccess {
    type: typeof LOAD_WEATHER_SUCEESS;
    payload: IWeather;
}

export interface LoadWeatherFail {
    type: typeof LOAD_WEATHER_FAIL;
    payload: any;
}

export const API_KEY = 'ea4a16757a0ed045d9b7995fd5e1ef1d';
export type AppThunk = ThunkAction<void, AppStore, null, Action<string>>;
export type WeatherActionTypes = LoadWeather | LoadWeatherSuccess | LoadWeatherFail | SetFetching;
